/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.exception;

import cn.hutool.core.exceptions.ExceptionUtil;
import com.je.common.base.result.BaseRespResult;
import com.je.connector.base.exception.ServerException;
import com.je.connector.base.exception.ServiceException;
import org.apache.servicecomb.swagger.invocation.Response;
import org.apache.servicecomb.swagger.invocation.SwaggerInvocation;
import org.apache.servicecomb.swagger.invocation.exception.ExceptionToProducerResponseConverter;

public class ConnectorServiceExceptionToProducerResponseConverter implements ExceptionToProducerResponseConverter<ServiceException> {

    @Override
    public Class<ServiceException> getExceptionClass() {
        return ServiceException.class;
    }

    @Override
    public Response convert(SwaggerInvocation swaggerInvocation, ServiceException e) {
        e.printStackTrace();
        BaseRespResult baseRespResult = BaseRespResult.errorResult("9999", "", ExceptionUtil.getMessage(e));
        return Response.succResp(baseRespResult);
    }

}
