package com.je.connector.server.prototype.handler;

import com.google.common.base.Strings;
import com.je.connector.base.connection.Connection;
import com.je.connector.base.message.MessageHandler;
import com.je.connector.base.message.model.KickUserMessage;
import com.je.connector.base.protocol.Packet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KickUserHandler implements MessageHandler {

    private static final Logger logger = LoggerFactory.getLogger(KickUserHandler.class);

    @Override
    public void handle(Packet packet, Connection connection) {
        logger.info("Received kick user message,the package is {}",packet);
        KickUserMessage kickUser = new KickUserMessage(packet,connection);
        kickUser.decodeBody();
        if(Strings.isNullOrEmpty(kickUser.userId) || Strings.isNullOrEmpty(connection.getUserId())){
            return;
        }

        if(kickUser.userId.equals(connection.getUserId())){
            connection.setUserId(null);
            logger.info("Kick user success,the user is {}",kickUser.userId);
        }else {
            logger.info("Kick user failed,becauseof the connection user is {},the user is {}",connection.getUserId(),kickUser.userId);
        }
    }

}
