/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.server.prototype.handler;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Charsets;
import com.je.connector.base.connection.Connection;
import com.je.connector.base.exception.handler.BaseMessageHandler;
import com.je.connector.base.message.model.*;
import com.je.connector.base.protocol.Packet;
import com.je.connector.server.InstantServer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PushHandler extends BaseMessageHandler<JsonBodyMessage> {


    private InstantServer instantServer;

    public PushHandler(InstantServer instantServer) {
        this.instantServer = instantServer;
    }

    @Override
    public JsonBodyMessage decode(Packet packet, Connection connection) {
        String json = new String(packet.getBody(), Charsets.UTF_8);
        JSONObject packetBodyObj = JSONObject.parseObject(json);
        if(MessageType.SYS_MESSAGE_TYPE.equals(packetBodyObj.getString("type"))){
            JECloudSystemMessage systemMessage = new JECloudSystemMessage(packet,connection);
            systemMessage.setBusKey(packetBodyObj.getString("busType"));
            systemMessage.setContent(packetBodyObj.getString("conent"));
            return systemMessage;
        }else if(MessageType.SCRIPT_MESSAGE_TYPE.equals(packetBodyObj.getString("type"))){
            JECloudScriptMessage scriptMessage = new JECloudScriptMessage(packet,connection);
            scriptMessage.setBusKey(packetBodyObj.getString("busType"));
            scriptMessage.setContent(packetBodyObj.getString("conent"));
            return scriptMessage;
        }else if(MessageType.NOREAD_MESSAGE_TYPE.equals(packetBodyObj.getString("type"))){
            JECloudNoReadMessage noReadMessage = new JECloudNoReadMessage(packet,connection);
            JECloudNoReadMessage.jsonToFields(noReadMessage,packetBodyObj);
            return noReadMessage;
        }else if(MessageType.NOTICE_MESSAGE_TYPE.equals(packetBodyObj.getString("type"))){
            JECloudNoticeMessage noticeMessage = new JECloudNoticeMessage(packet,connection);
            noticeMessage.jsonToFields(packetBodyObj);
            return noticeMessage;
        }
        return null;
    }

    @Override
    public void handle(JsonBodyMessage message) {
        try {
            BaseMessage.buildSuccessResponse(message.getConnection()).channelSend(null);
        } catch (Throwable e) {
            BaseMessage.buildErrorResponse(message.getConnection()).channelSend(null);
            log.error("handle failure!", e);
        }
    }

}
