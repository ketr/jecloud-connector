/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.server.registry;

import com.je.connector.base.connection.Connection;
import com.je.connector.base.connection.channel.SocketChannel;
import com.je.connector.base.connection.registry.ConnectionRegistry;
import com.je.connector.base.exception.ServerException;
import com.je.connector.server.InstantServer;
import org.apache.servicecomb.core.SCBEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

public class ClusterRedisConnectionRegistry implements ConnectionRegistry {

    private static final Logger logger = LoggerFactory.getLogger(ClusterRedisConnectionRegistry.class);

    private InstantServer instantServer;

    private LocalConnectionRegistry localConnectionRegistry = new LocalConnectionRegistry();

    public ClusterRedisConnectionRegistry(InstantServer instantServer) {
        this.instantServer = instantServer;
    }

    @Override
    public List<Connection> getConnections() {
        throw new ServerException("集群环境下不支持获取所有连接！");
    }

    @Override
    public List<Connection> getLocalConnections() {
        return localConnectionRegistry.getConnections();
    }

    @Override
    public boolean localExist(Connection connection) {
        return localConnectionRegistry.exist(connection);
    }

    @Override
    public boolean exist(Connection connection) {
        return instantServer.getConnectionRegistryRpcService().exist(connection.getId());
    }

    @Override
    public void add(Connection connection) {
        String appId = SCBEngine.getInstance().getAppId();
        localConnectionRegistry.add(connection);
        instantServer.getConnectionRegistryRpcService().putAppAndConnectId(appId,connection.getId());
    }

    @Override
    public void putUser(Connection connection) {
        localConnectionRegistry.putUser(connection);
        instantServer.getConnectionRegistryRpcService().putUserAndConnectorId(connection.getUserId(),connection.getDeviceId(),connection.getId());
    }

    @Override
    public Connection get(SocketChannel clientChannel) {
        return localConnectionRegistry.get(clientChannel);
    }

    @Override
    public Connection removeAndClose(SocketChannel clientChannel) {
        String appId = SCBEngine.getInstance().getAppId();
        Connection connection = localConnectionRegistry.removeAndClose(clientChannel);
        instantServer.getRedisTemplate().opsForHash().delete(appId,connection.getId());
        return connection;
    }

    @Override
    public long getConnectionNum() {
        return instantServer.getConnectionRegistryRpcService().size();
    }

    @Override
    public void init() {
        logger.debug("init cluster connection registry!");
    }

    @Override
    public void destroy() {
        logger.debug("destroy cluster connection Registry!");
    }
}
