/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.server.listener;

import com.je.common.base.message.vo.PushMessage;
import com.je.connector.base.config.InstantConfig;
import com.je.connector.server.InstantServer;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: jecloud-message
 * @author: LIULJ
 * @create: 2021/8/26
 * @description: 默认监听器实现，此监听用于应用服务端发送消息到连接管理器，此监听配置需要使用SPI注册机制，请注意，否则不生效
 */
@Slf4j
public class DefaultTopicListener implements TopicListener {

    private InstantServer instantServer;

    public DefaultTopicListener(InstantServer instantServer) {
        this.instantServer = instantServer;
    }

    @Override
    public String getPattern() {
        return InstantConfig.server.jmx.topic;
    }

    @Override
    public void onMessage(PushMessage message) {
        log.info("消费redis消息队列，发送消息，{}", message.toString());
        instantServer.getMessageService().sendMsg(message.getTargetUserIds(), message);
    }

}
