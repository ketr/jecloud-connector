/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.server.prototype.handler;

import com.alibaba.fastjson2.JSON;
import com.google.common.base.Strings;
import com.je.connector.base.connection.Connection;
import com.je.connector.base.exception.handler.BaseMessageHandler;
import com.je.connector.base.message.model.HandshakeMessage;
import com.je.connector.base.protocol.Packet;
import com.je.connector.base.utils.ErrorCode;
import com.je.connector.server.InstantServer;
import com.je.connector.util.SessionContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * 握手消息处理器
 *
 * @ProjectName: instant-message
 * @Package: com.connector.server.handler
 * @ClassName: HandshakeHandler
 * @Description: 定义握手消息处理器
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Slf4j
public class HandshakeHandler extends BaseMessageHandler<HandshakeMessage> {

    private InstantServer instantServer;

    public HandshakeHandler(InstantServer instantServer) {
        this.instantServer = instantServer;
    }

    @Override
    public HandshakeMessage decode(Packet packet, Connection connection) {
        return new HandshakeMessage(packet, connection);
    }

    @Override
    public void handle(HandshakeMessage message) {
        //1.校验客户端消息字段
        if (Strings.isNullOrEmpty(message.getDeviceId())) {
            HandshakeMessage.buildErrorResponse(message.getConnection()).setErrorCode(ErrorCode.INVALID_DEVICE).buildResult("Param invalid", "没有找到设备标识！").close();
            log.error("handshake failure, message={}, conn={}", message, message.getConnection());
            return;
        }

        if (StringUtils.isEmpty(message.getUserId())) {
            HandshakeMessage.buildErrorResponse(message.getConnection()).setErrorCode(ErrorCode.UNKNOWN).buildResult("Param invalid", "用户id为空,握手失败！").close();
        }

        String mobile = message.getMobile();
        if (StringUtils.isEmpty(mobile) || mobile.equals("1")) {
            message.setMobile("1");
        } else {
            message.setMobile("2");
        }

        //8.保存client信息到当前连接
        Connection connection = message.getConnection();
        connection.setSessionContext(SessionContextUtil.buildSessionContext(message));
        connection.setUserId(message.getUserId());
        instantServer.getConnectionRegistry().putUser(connection);
        log.info("握手成功,handshakeMessage:{}", JSON.toJSONString(message));
        log.info("握手成功,往连接器中添加当前连接:{}", connection.getUserId());
        log.info("握手成功,当前连接器中的数量：{}", instantServer.getConnectionRegistry().getLocalConnections().size());

    }
}

