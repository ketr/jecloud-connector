/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.rpc;

import java.util.List;

public interface ConnectionRegistryRpcService {

    /**
     * APP映射表
     */
    String APP_MAP_KEY = "jecloud/push/app";
    /**
     * 前缀
     */
    String APP_CONNECTION_MAP_KEY_PREFIX = "jecloud/push/appConnectorMap/";
    /**
     * redis中连接映射表
     * subKey: 微服务实例标识
     * value: 连接id
     */
    String APP_CONNECTION_MAP_KEY_TEMPLAT = APP_CONNECTION_MAP_KEY_PREFIX + "%s";
    /**
     * 前缀
     */
    String USER_CONNECTION_MAP_KEY_PREFIX = "jecloud/push/userConnectorMap/";
    /**
     * redis中用户连接映射表
     * value: 连接id
     */
    String USER_CONNECTION_MAP_KEY_TEMPLAT = USER_CONNECTION_MAP_KEY_PREFIX + "%s-%s";

    /**
     * 是否存在此链接
     * @param connectionId
     * @return
     */
    boolean exist(String connectionId);

    void putApp(String appId);

    /**
     * 在Redis中存放了一个应用实例-链接ID的Hash表
     * @param appId
     * @param connectorId
     */
    void putAppAndConnectId(String appId,String connectorId);

    /**
     * 在Redis中存放了一个用户-链接ID的Hash表
     * @param userId
     * @param connectorId
     */
    void putUserAndConnectorId(String userId,String deviceId,String connectorId);

    /**
     * 获取连接所在服务实例
     * @param connectorId
     */
    String getAppIdByConnector(String connectorId);

    /**
     * 根据
     * @param userId
     * @return
     */
    String getAppIdByUser(String userId,String deviceId);

    /**
     * 获取所有的APPID
     * @return
     */
    List<String> getAllApps();

    /**
     * 获取APP的所有连接ID
     * @param appId
     * @return
     */
    List<String> getAppConnectors(String appId);

    long size();
}
