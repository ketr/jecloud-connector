/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.message.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 通知
 */
public class Notice implements Serializable {

    /**
     * 持续显示时间
     */
    private long duration;
    /**
     * 是否可关闭
     */
    private boolean closeable;
    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    /**
     * 位置
     */
    private String placement;
    /**
     * 状态，成功|失败等，业务可以指定
     */
    private String status;
    /**
     * 声音
     */
    private boolean playAudio;
    /**
     * 时间
     */
    private String nowTime;
    /**
     * 通知按钮
     */
    private List<NoticeButton> buttons;

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean isCloseable() {
        return closeable;
    }

    public void setCloseable(boolean closeable) {
        this.closeable = closeable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isPlayAudio() {
        return playAudio;
    }

    public void setPlayAudio(boolean playAudio) {
        this.playAudio = playAudio;
    }

    public String getNowTime() {
        return nowTime;
    }

    public void setNowTime(String nowTime) {
        this.nowTime = nowTime;
    }

    public List<NoticeButton> getButtons() {
        return buttons;
    }

    public void setButtons(List<NoticeButton> buttons) {
        this.buttons = buttons;
    }

    /**
     * 通知按钮
     */
    public static final class NoticeButton {

        private String text;
        private String icon;
        private String iconColor;
        private String bgColor;
        private String borderColor;
        private String fontColor;
        private String script;
        private Map<String,Object> params;

        public NoticeButton() {
        }

        public NoticeButton(String text, String icon, String iconColor, String bgColor, String borderColor, String fontColor, String script, Map<String, Object> params) {
            this.text = text;
            this.icon = icon;
            this.iconColor = iconColor;
            this.bgColor = bgColor;
            this.borderColor = borderColor;
            this.fontColor = fontColor;
            this.script = script;
            this.params = params;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getIconColor() {
            return iconColor;
        }

        public void setIconColor(String iconColor) {
            this.iconColor = iconColor;
        }

        public String getBgColor() {
            return bgColor;
        }

        public void setBgColor(String bgColor) {
            this.bgColor = bgColor;
        }

        public String getBorderColor() {
            return borderColor;
        }

        public void setBorderColor(String borderColor) {
            this.borderColor = borderColor;
        }

        public String getFontColor() {
            return fontColor;
        }

        public void setFontColor(String fontColor) {
            this.fontColor = fontColor;
        }

        public String getScript() {
            return script;
        }

        public void setScript(String script) {
            this.script = script;
        }

        public Map<String, Object> getParams() {
            return params;
        }

        public void setParams(Map<String, Object> params) {
            this.params = params;
        }

        @Override
        public String toString() {
            return "NoticeButton{" +
                    "text='" + text + '\'' +
                    ", icon='" + icon + '\'' +
                    ", iconColor='" + iconColor + '\'' +
                    ", bgColor='" + bgColor + '\'' +
                    ", borderColor='" + borderColor + '\'' +
                    ", fontColor='" + fontColor + '\'' +
                    ", script='" + script + '\'' +
                    ", params=" + params +
                    '}';
        }

    }

}
