/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.message.model;

import com.alibaba.fastjson2.JSONObject;
import com.je.connector.base.connection.Connection;
import com.je.connector.base.protocol.Command;
import com.je.connector.base.protocol.Packet;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 踢掉用户消息
 *
 * @ProjectName: instant-message
 * @Package: com.connector.base.message.model
 * @ClassName: KickUserMessage
 * @Description: 踢掉用户消息
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public final class KickUserMessage extends JsonBodyMessage {

    public String deviceId;
    public String userId;

    public KickUserMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void jsonToFields(JSONObject jsonBody) {
        deviceId = jsonBody.containsKey("deviceId") ? jsonBody.getString("deviceId") : null;
        userId = jsonBody.containsKey("userId") ? jsonBody.getString("userId") : null;
    }

    @Override
    public JSONObject fieldToJson() {
        JSONObject resultObj = new JSONObject();
        resultObj.put("deviceId", deviceId);
        resultObj.put("userId", userId);
        return resultObj;
    }

    /**
     * 构建响应
     *
     * @return
     * @throws Exception
     */
    public KickUserMessage buildResponse() throws Exception {
        KickUserMessage kickRespMessage = null;
        if (Packet.NORMAL_REQ_TYPE == packet.getType()) {
            kickRespMessage = new KickUserMessage(new Packet(Command.KICK.cmd, Packet.NORMAL_RESP_TYPE), connection);
        } else if (Packet.CLUSTER_REQ_TYPE == packet.getType()) {
            kickRespMessage = new KickUserMessage(new Packet(Command.KICK.cmd, Packet.CLUSTER_RESP_TYPE), connection);
        }
        if (kickRespMessage == null) {
            throw new Exception("unknown package request type");
        }
        return kickRespMessage;
    }

    public static KickUserMessage build(Connection connection) {
        return new KickUserMessage(new Packet(Command.KICK.cmd), connection);
    }


}
