/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.message.model;

import com.alibaba.fastjson2.JSONObject;
import com.je.connector.base.connection.Connection;
import com.je.connector.base.protocol.Packet;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 成功消息定义
 *
 * @ProjectName: instant-message
 * @Package: com.connector.base.message.model
 * @ClassName: OkMessage
 * @Description: 成功消息定义
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public final class OkMessage extends JsonBodyMessage {

    public String type;
    public String content;

    public OkMessage setType(String type) {
        this.type = type;
        return this;
    }

    public OkMessage setData(String data) {
        this.type = "0";
        this.content = data;
        return this;
    }

    public OkMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void jsonToFields(JSONObject jsonBody) {
        type = jsonBody.containsKey("type") ? jsonBody.getString("type") : null;
        content = jsonBody.containsKey("data") ? jsonBody.getString("data") : null;
    }

    @Override
    public JSONObject fieldToJson() {
        JSONObject resultObj = new JSONObject();
        resultObj.put("type", type);
        resultObj.put("content", content);
        return resultObj;
    }

}
