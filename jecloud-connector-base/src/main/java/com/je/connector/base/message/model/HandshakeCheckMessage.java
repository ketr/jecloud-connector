/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.message.model;

import com.alibaba.fastjson2.JSONObject;
import com.je.connector.base.connection.Connection;
import com.je.connector.base.protocol.Command;
import com.je.connector.base.protocol.Packet;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class HandshakeCheckMessage extends JsonBodyMessage {

    private String msg;

    public HandshakeCheckMessage(Connection connection) {
        super(new Packet(Command.HANDSHAKECHECK.cmd, Packet.NORMAL_REQ_TYPE, genSessionId()), connection);
    }

    public HandshakeCheckMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void jsonToFields(JSONObject jsonBody) {
        msg = jsonBody.containsKey("msg") ? jsonBody.getString("msg") : null;
    }

    @Override
    public JSONObject fieldToJson() {
        JSONObject resultObj = new JSONObject();
        resultObj.put("message", msg);
        return resultObj;
    }

    public static HandshakeMessage buildResponse(BaseMessage src) {
        HandshakeMessage response = null;
        if (Packet.NORMAL_REQ_TYPE == src.packet.getType()) {
            response = new HandshakeMessage(new Packet(Command.HANDSHAKECHECK.cmd, Packet.NORMAL_RESP_TYPE, src.packet.getSessionId()), src.connection);
        } else if (Packet.CLUSTER_REQ_TYPE == src.packet.getType()) {
            response = new HandshakeMessage(new Packet(Command.HANDSHAKECHECK.cmd, Packet.CLUSTER_RESP_TYPE, src.packet.getSessionId()), src.connection);
        }
        return response;
    }

    protected static int genSessionId() {
        return ID_SEQ.incrementAndGet();
    }
}
