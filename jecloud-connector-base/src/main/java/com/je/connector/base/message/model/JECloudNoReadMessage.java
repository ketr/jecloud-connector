/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.message.model;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Charsets;
import com.je.connector.base.connection.Connection;
import com.je.connector.base.protocol.Command;
import com.je.connector.base.protocol.Packet;
import java.util.ArrayList;
import java.util.List;

/**
 * 未读消息
 */
public class JECloudNoReadMessage extends JECloudMessage<List<JECloudMessage>> {

    public JECloudNoReadMessage(Packet packet, Connection connection) {
        super(packet, connection);
        this.type = MessageType.NOREAD_MESSAGE_TYPE;
    }

    public JECloudNoReadMessage(String busType) {
        this.type = MessageType.NOREAD_MESSAGE_TYPE;
        this.busKey = busType;
    }

    public JECloudNoReadMessage(String busType, List<JECloudMessage> content) {
        this.type = MessageType.NOREAD_MESSAGE_TYPE;
        this.busKey = busType;
        this.content = content;
    }

    public static void jsonToFields(JECloudNoReadMessage noReadMessage,JSONObject jsonBody){
        noReadMessage.setType(jsonBody.getString("type"));
        noReadMessage.setBusKey(jsonBody.getString("busType"));
        JSONArray array = jsonBody.getJSONArray("content");
        JSONObject eachJsonObj;
        List<JECloudMessage> contents = new ArrayList<>();
        JECloudMessage eachMessage;
        for (int i = 0; i < array.size(); i++) {
            eachJsonObj = array.getJSONObject(i);
            if(MessageType.SYS_MESSAGE_TYPE.equals(eachJsonObj.getString("type"))){
                eachMessage = new JECloudSystemMessage(eachJsonObj.getString("busType"),eachJsonObj.getString("content"));
            }else if(MessageType.SCRIPT_MESSAGE_TYPE.equals(eachJsonObj.getString("type"))){
                eachMessage = new JECloudScriptMessage(eachJsonObj.getString("busType"),eachJsonObj.getString("content"));
                eachMessage.jsonToFields(eachJsonObj);
            }else if(MessageType.NOTICE_MESSAGE_TYPE.equals(eachJsonObj.getString("type"))){
                eachMessage = new JECloudNoticeMessage();
                eachMessage.jsonToFields(jsonBody);
            }else{
                eachMessage = new JECloudNoReadMessage(eachJsonObj.getString("busType"));
                jsonToFields((JECloudNoReadMessage) eachMessage,eachJsonObj);
            }
            contents.add(eachMessage);
        }
        noReadMessage.setContent(contents);
    }

    @Override
    public void jsonToFields(JSONObject jsonBody) {
        jsonToFields(this,jsonBody);
    }

    @Override
    public JSONObject fieldToJson() {
        JSONObject messageObj = new JSONObject();
        messageObj.put("type", getType());
        messageObj.put("busType", getBusKey());
        List<JECloudMessage> contents = getContent();
        JSONArray array = new JSONArray();
        for (JECloudMessage eachMessage : contents) {
            array.add(eachMessage.fieldToJson());
        }
        messageObj.put("content", array.toJSONString());
        return messageObj;
    }

    public static Packet buildPacket(JECloudMessage msg) {
        JSONObject messageObj = msg.fieldToJson();
        String message = messageObj.toJSONString();

        Packet packet = new Packet();
        packet.setCmd(Command.PUSH.cmd);
        packet.setBodyLength(message.length());
        packet.setType((byte) 1);
        packet.setBody(message.getBytes(Charsets.UTF_8));
        return packet;
    }

}
