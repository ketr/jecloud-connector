/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.connection;

import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 定义会话
 *
 * @ProjectName: instant-message
 * @Package: com.connector.base
 * @ClassName: SessionContext
 * @Description: 定义会话
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Getter
@ToString
public class SessionContext implements Serializable {

    /**
     * 操作系统名称
     */
    private String osName;
    /**
     * 操作系统版本
     */
    private String osVersion;
    /**
     * 设备ID
     */
    private String deviceId;
    /**
     * 客户端版本
     */
    private String clientVersion;
    /**
     * 客户端类型
     */
    private String clientType;
    /**
     * 用户打标
     */
    private String tags;
    /**
     * 10s
     */
    private int heartbeat = 10000;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 是否移动设备,默认不是1:不是,2:是
     */
    private String mobile;
    /**
     * 是否通过验证 如果超过两分钟就是没有通过
     */
    private long time1 = 0;
    private long time2 = 0;
    //为了判断多个设备问题
    private String uuid;

    private String appid;

    private String cid;

    public SessionContext() {
    }

    /**
     * 构建会话上下文
     *
     * @return
     */
    public static SessionContext build() {
        return new SessionContext();
    }

    public SessionContext setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public SessionContext setOsName(String osName) {
        this.osName = osName;
        return this;
    }

    public SessionContext setOsVersion(String osVersion) {
        this.osVersion = osVersion;
        return this;
    }

    public SessionContext setDeviceId(String deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public SessionContext setClientType(String clientType) {
        this.clientType = clientType;
        return this;
    }

    public SessionContext setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
        return this;
    }

    public SessionContext setTags(String tags) {
        this.tags = tags;
        return this;
    }

    public SessionContext setHeartbeat(int heartbeat) {
        this.heartbeat = heartbeat;
        return this;
    }

    public SessionContext setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public SessionContext setTime1(long time1) {
        this.time1 = time1;
        return this;
    }

    public SessionContext setTime2(long time2) {
        this.time2 = time2;
        return this;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppid() {
        return appid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCid() {
        return cid;
    }
}
